/*
 * Imsai 8080-style simulation
 * Z80 hardware-in-the-loop
 *
 * njm Jan 2021
 *
 */

// ATMEGA 328P I/O setup

// Databus Serialization
#define DBLOAD 2   // load databus into reg (active low)
#define DBSHIFT 3  // shift loaded data
#define DBOUT 4    // read shifted data
#define DBIN 5     // serial input to shift register *** KEEP THIS HIGH EXCEPT WHEN WRITING DBUS ***
#define DBDRIVE 6  // send output shift reg to databus (active low)

// serial input data - switches and status
#define SRLOAD 7   // load switches and status into SR (active low) *** KEEP THIS HIGH EXCEPT WHEN NEEDED ***
#define SRSHIFT 8  // shift loaded data
#define SROUT 9    // read shifted data

// D10 reserved; D11 MOSI; D12 MISO; D13 SCK
// Useful for flash memory/bootstrapping CPU

// ATMEGA Override of Z80 Signals
#define BLOCK A0   // prevent Z80's #WR and #RD from reaching SRAM
#define CPUWR A1   // drive #WR when BLOCK is active (active low)
#define XBLOCK A2  // prevent crystal from driving Z80's clock
#define CPUXTAL A3 // drive Z80's clockl when XBLOCK active

// Address/Data/User Input switches to DataBus
#define SWF8EN A4  // active low
#define SW70EN A5  // active low

// Counts for interesting signals from the 12-bit shift register
// do #SRLOAD, then this many SRSHIFTs before reading SROUT
#define MREQ 0
#define WR 1
#define RD 2
#define M1 3
#define DEPNEXT 4
#define DEP 5
#define EXNEXT 6
#define EX 7
#define STEP 8
#define STOP 9
#define RUN 10
#define RESET 11

//
// We need to drive #RESET from code. We're going to use (DBIN OR SRLOAD) to feed #RESET
//

//
// SINGLE-STEP MODE
//
#define STEP_M 0  // step from MREQ+RD to MREQ+RD
#define STEP_M1 1 // Step (safely) from M1 to m1
#define STEP_T 2  // just tick the clock (H->L->H)

byte stepMode=STEP_M;
byte RegA,RegB,RegC,RegD,RegE,RegH,RegL,RegF; // set these in readPC()
int RegSP;

char lastCmd[32]; // save prior command

byte running=9; // 1 means CPU is free-running
               // 0 means things are paused, waiting for user input

// busShowFlag values
#define BUSALLFLAG 2 // signals + registers
#define BUSSIGFLAG 1 // just memory signals
#define BUSNONEFLAG 0
byte busShowFlag=BUSNONEFLAG; // default case 

//
// MULTI-BYTE OPCODE MANAGEMENT (7 Feb 2021)
//
// We don't want to manipulate the Z80 while it's in the middle of
// processing an instruction. Normally we can just check for M1 and MREQ and RD;
// but in a multi-byte opcode, those signals are active for the second byte's fetch
// as well as the first. If we mistakenly force, say, a C3 onto the bus at that
// point, we would be interfering with the completion of that instruction.
// 
// PLAN:
// When CPU stops, set MBFlag = 2
// In doTick(), after ticking the clock, see if we're at OCF (Opcode Fetch)
// If not, then no special action is required.
// But if we are at ICF, check MBOpcode(). If true, set MBFlag=2
//        else decrement MBFlag down to 0)
//
// We're SAFE whenever MBFlag==0 and we're at OCF
//
//
// NOTE: we call doTick() a lot; for example, while doing a dump command,
// or reading the PC, or examining memory.
// During these actions, we don't want to change MBFlag. So calling doTick(0) means
// don't update the flag.
//

byte MBFlag=0; // Multi-Byte flag. Managed by doTick()

// flash code is long, so only do it once
// pass a flag to IOFlash() as first argument
#define READFLAG 0
#define WRITEFLAG 1

// main loop - read serial input, and on \n process a command
void loop()
{
  processSwitches(); // pick up any switch changes

  if (doCmd()){ // a command was processed
    //showSignals(busShowFlag); // if reading PC, this will move us to a new M1 cycle
  }
}

// only call this when CPU is stopped!
void doTick(int updateMBFlag)
{
// So before we tick, let's see if this tick will cause the fetch of a
// multi-byte opcode. If so, set MBFlag to 2.
//
// After the tick, see if we're at an opcode fetch, and if so,
// decrement MBFlag (if set)

// ignore the update flag???
  if (updateMBFlag==0){ // yes - just tick (we're doing something internal)
    digitalWrite(CPUXTAL,LOW);
    digitalWrite(CPUXTAL,HIGH);
    return;
  }

  if (OCFetch() && MBOpcode(readDataBus())){ // about to fetch e.g. DD
    MBFlag=2;
  }

// tick the clock
  digitalWrite(CPUXTAL,LOW);
  digitalWrite(CPUXTAL,HIGH);

// now see where we are safety-wise
  if (MBFlag && OCFetch() && (!MBOpcode(readDataBus()))) --MBFlag;

  //Serial.print("At OCCFtech; dbus=");Serial.print(readDataBus(),HEX);Serial.print(" MBFlag=");Serial.println(MBFlag);

  return;
}

// single step - one M cycle or one T cycle

/*** DANGER!!! ***/
// Watch out for multi-byte opcodes.
// These are fine for, say, examine commands;
// but if we're planning to let the Z80 execute instructions
// (e.g. while single-stepping), we need to make sure we don't run the readPC() stuff
// in the middle of (for example) a CB2C instruction opcode(s) fetch.
//
// Have getToOCFetch(1) set a flag saying if we're seeing a prefix
// If so, this step instruction will skip the bus read, and clear that flag
//

void singleStep()
{
  int tries=0;

  if (stepMode==STEP_T){ // just toggle the clock
    doTick(1);
  } else if (stepMode==STEP_M){
    MStep(1); // step a full M cycle
  } else M1Step(); // safe step to next instruction start (M1)
}

// user wants to do a full M-cycle
void MStep(int canShow)
{
  int tries;

// (don't check for #MREQ since single-step should stop during an #IORQ too)
  while ((++tries < 1000) && memRequest()) doTick(1); // tick till no memory request
  errorCheck(1,tries);
  tries=0;
  while ((++tries < 1000) && !memRequest()) doTick(1); // then tick till next request
  errorCheck(2,tries);

// now we're at a memory or IO request
  if (!canShow) return; // we're calling this from inside M1Step();

  showDataAfterStep();
}

void showDataAfterStep()   // after doing MStep() or M1Step() let's try to show stuff
{
  if (busShowFlag == BUSNONEFLAG) return; // no worries...
  if (busShowFlag == BUSSIGFLAG){
    showSignals(busShowFlag);
    return;
  }

// try to show all data...
  if (!MBSafe()){
    showSignals(BUSSIGFLAG); // don't show PC
    return;
  }

// safe to show all :)
  showSignals(BUSALLFLAG);
}

// step a (safe) M1 cycle
void M1Step()
{
  int i; // # of tries to find safe spot

  for (i=0;i<10;i++){
    MStep(0); // 0 means MStep() should not show bus and register data...
    if (MBSafe()) break;
  }
  if (i==10){
    Serial.println(F("Can't park"));
  } else {
    showDataAfterStep();
  }
}

void errorCheck(int where, int tries)
{
  if (tries != 1000) return; // didn't time out
  Serial.print(where);
  Serial.println(F(": TIMEOUT!"));
}

// shift register support code
int srLoad()
{
  digitalWrite(SRLOAD,LOW);
//delay(1);
  digitalWrite(SRLOAD,HIGH); // load
//delay(1);
  return(digitalRead(SROUT));
}

int srShift()
{
  digitalWrite(SRSHIFT,HIGH); // shift
//delay(1);
  digitalWrite(SRSHIFT,LOW);
//delay(1);
  return(digitalRead(SROUT));
}

// build databus value (load and shift)
int readDataBus()
{
  digitalWrite(DBLOAD,LOW);digitalWrite(DBLOAD,HIGH); // load databus into SR
  int out=0;
  for (int i=7;i>=0;i--){ // read bit i
    int val=digitalRead(DBOUT); // current bit
    if (val) out = out | (1<<i);
    digitalWrite(DBSHIFT,HIGH);digitalWrite(DBSHIFT,LOW); // shift databus info 
  }
  return(out);
}

void writeDataBus(int data)
{
  for (int i=7;i>=0;i--){ // shift in bit i
    digitalWrite(DBIN,(data&(1<<i))?HIGH:LOW);
    digitalWrite(DBSHIFT,HIGH);digitalWrite(DBSHIFT,LOW);
  }
// the register clock lags the shift clock by one tick (when tied together)\
// so do one more tick
  digitalWrite(DBSHIFT,HIGH);digitalWrite(DBSHIFT,LOW);
  digitalWrite(DBIN,HIGH); // this needs to normally sit HIGH
}

// call writeDataBus before claiming bus
void claimDataBus()
{

// note that the Arduino never requests a memory read from the SRAM
// when we want to read SRAM, we coax the Z80 into reading the desired location
// and then just peek the bus

  digitalWrite(BLOCK,HIGH); // cause #RD and #WR from Z80 to be ignored
  digitalWrite(DBDRIVE,LOW); // place (pre-loaded) shift register's contents onto the databus
}

void releaseDataBus()
{
  digitalWrite(DBDRIVE,HIGH); // remove shift register's contents from databus
  digitalWrite(BLOCK,LOW); // Let Z80 interact with the memory subsystem
}

// check for input, build a command, process it

int inLoc=0;
char buffer[32];

int doCmd()
{
  char c;
  if (!Serial.available()) return(0); // nothing here
  
  c=Serial.read(); // get char


  if (crlf(c)) return(0); // <LF> following <CR> is ignored

  if (c=='\r') c='\n'; // standardize end of line char

  if ((c=='\b') || (c==127)){ // backspace or DEL
    if (inLoc > 0){
      --inLoc; // back up
      Serial.print(c);
    }
    return(0);
  }

  if (c==21){ // ^U
    for (int i=0;i<inLoc;i++) Serial.print(F("\b \b")); // erase chars :)
    inLoc=0;
    return(0);
  }

  if (c != '\n'){  // add to running string
    Serial.print(c);
    buffer[inLoc++]=c;
  } else {
    Serial.println();
  }
  
  if (inLoc==31 || c=='\n'){ // end of line
    buffer[inLoc]='\0';
    inLoc=0;
    process(buffer);
    doPrompt();
    return(1);
  }
  return(0);
}

void doPrompt()
{
  //Serial.print("safe: ");Serial.print(MBSafe());Serial.print(" MBFlag=");Serial.print(MBFlag);
  if (MBSafe()){
    Serial.print(F("> ")); // new prompt
  } else {
    Serial.print(F("? "));
  }
}

int sawCR=0; // set when we see <CR>
// return 1 if c is <LF> and prior char was <CR>
int crlf(char c)
{
  if (sawCR && (c=='\n')){
    sawCR=0;
    return(1); // just got <CR><LF>
  }
  sawCR=(c=='\r');
  return(0);
}

// INIT
void setup() {

// condition inputs and outputs

  digitalWrite(DBLOAD,HIGH);pinMode(DBLOAD,OUTPUT);
  digitalWrite(DBSHIFT,LOW);pinMode(DBSHIFT,OUTPUT);
  pinMode(DBOUT,INPUT); // FROM shift register
  digitalWrite(DBIN,HIGH); pinMode(DBIN,OUTPUT); // TO shift register - KEEP THIS HIGH
  digitalWrite(DBDRIVE,HIGH);pinMode(DBDRIVE,OUTPUT);

  digitalWrite(SRLOAD,HIGH); pinMode(SRLOAD,OUTPUT);
  digitalWrite(SRSHIFT,LOW); pinMode(SRSHIFT,OUTPUT);
  pinMode(SROUT,INPUT); // FROM shift register

  digitalWrite(BLOCK,LOW);pinMode(BLOCK,OUTPUT);
  digitalWrite(CPUWR,HIGH);pinMode(CPUWR,OUTPUT); // while not blocking, this signal must be HIGH
  digitalWrite(XBLOCK,LOW);pinMode(XBLOCK,OUTPUT);
  digitalWrite(CPUXTAL,HIGH);pinMode(CPUXTAL,OUTPUT); // must be HIGH while not blocking clock

  digitalWrite(SWF8EN,HIGH);pinMode(SWF8EN,OUTPUT);
  digitalWrite(SW70EN,HIGH);pinMode(SW70EN,OUTPUT);

// databus is released by (part of) the above

  Serial.begin(115200);

// STOP SYSTEM
  running=0;
  digitalWrite(CPUXTAL,HIGH);
  digitalWrite(XBLOCK,HIGH);
  delay(50); // let the clock-FF go high
  //delay(1000);
  doReset();
  //jump(0);
  //delay(1000);
  //doReset();

// initialize flash chip
  flashInit();

// start comm w/user
  Serial.println(F("Ready"));

// set saved command
  strcpy(lastCmd,"___"); // will display unknown-command-message

// prompt
  doPrompt();

/*** DEBUG count values onto database ***/
/***
  digitalWrite(BLOCK,HIGH);
  digitalWrite(XBLOCK,HIGH);
  int i=0;
  while (1){
    Serial.println(i);
    releaseDataBus(); // so we can shift bits in
    writeDataBus(i);
    claimDataBus(); // send SR output to databus
    delay(125);
    i++;
  }
***/

// next, send A7-A0 switches to database

/*** DEBUG just show serialized data :) ***/
/***
  while(1){
    Serial.print(srLoad());
    for (int i=0;i<11;i++){
      Serial.print(srShift());
    }
    Serial.println();
  }
***/
//return;


  //specialDebug(); // do slow count and send to output port
  return;
}

// move to next location (execute NOP)
void nextLoc()
{
  getToOCFetch(1); // presumably already here

// force a NOP
  writeDataBus(0); // NOP opcode
  claimDataBus();
  getToOCFetch(0); // move beyond fetch state
  releaseDataBus();

  getToOCFetch(1); // when we're at OC fetch again, we should be at the next address
}

// load a value into memory
void load(int data)
{
  allLoad(data,0); // don't use switches
}

void swLoad()
{
  allLoad(0,1); // use switches
}

// general load program
void allLoad(int data, int useSwitches) // set useSwitches to 1 to read
                                       // desired data value from switches
{
  getToOCFetch(1); // wait till we can claim the bus
  if (useSwitches){
    digitalWrite(BLOCK,HIGH); // cause #RD and #WR from Z80 to be ignored
    digitalWrite(SW70EN,LOW); // and supply data from the switches
  } else { // drive databus from software (here)
    writeDataBus(data);//delay(100);
    claimDataBus();
  }

  digitalWrite(CPUWR,LOW);//delay(100); // WRITE PULSE!
  digitalWrite(CPUWR,HIGH);//delay(100);

// done with the write!
  digitalWrite(SW70EN,HIGH); // block tri-state buffer
  releaseDataBus();
}

// jump to a given location
void jump(int addr)
{
  allJump(addr,0); // use allJump routine: 2nd argument ("useSwitches") says to drive databus with destination address
}

// jump to location given by switches
void swJump()
{
  allJump(0,1); // 1 means use switches instead of argument
}


void allJump(int addr, int useSwitches)
{
  int high,low;
  low=addr & 0xff;
  high=(addr>>8) & 0xff;

// tick until we're at an opcode fetch
  getToOCFetch(1);

// force a JMP opcode
  writeDataBus(0xc3); // JMP opcode
  claimDataBus();

  getToOCFetch(0); // move beyond fetch
  releaseDataBus();

//Serial.println("In jump, looking for MRead(1)");
  getToMRead(1); // get to mem read request
//Serial.println("In jump, found MRead(1)");

// Z80 is reading memory (2nd byte of instruction: the operand)
  if (useSwitches){ // just route physical switches to the databus
    digitalWrite(BLOCK,HIGH); // cause #RD and #WR from Z80 to be ignored
    digitalWrite(SW70EN,LOW);
  } else { // else drive databus from here
    writeDataBus(low); // low byte of jump address
    claimDataBus();
  }

  getToMRead(0); // finish read state

  digitalWrite(SW70EN,HIGH); // block tri-state buffer
  releaseDataBus();

  getToMRead(1); // wait till Z80 is reading final byte
  
// same drill here...
  if (useSwitches){ // route high-byte of physical switches to the databus
    digitalWrite(BLOCK,HIGH); // cause #RD and #WR from Z80 to be ignored
    digitalWrite(SWF8EN,LOW);
  } else { // else drive databus from here
    writeDataBus(high); // high byte of jump address
    claimDataBus();
  }

  getToMRead(0); // and wait till read is finished

  digitalWrite(SWF8EN,HIGH); // block tri-state buffer
  releaseDataBus();

// Z80 should be processing JMP instruction

  getToOCFetch(1); // when we're at OC fetch again, we should be at target address
}

// code to back up (after disassem, we've skipped forward a few bytes)
void backup(int amount)
{
  amount=2+amount;
  getToOCFetch(1);
  writeDataBus(0x18); // JR
  claimDataBus();getToOCFetch(0);releaseDataBus();
  getToMRead(1);
  writeDataBus(-amount);
  claimDataBus();
  getToMRead(0);
  releaseDataBus();
  getToOCFetch(1);
}

// code to read current PC
// this is fun :)
int readPC()
{
  getToOCFetch(1); // Z80 is trying to fetch an instruction
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(0xCD); // opcode for CALL
  claimDataBus();
  getToOCFetch(0); // wait till CALL has been ingested
  getToMRead(1); // read LSB of dest (doesn't matter where this is, let it be CD)
  getToMRead(0); // LSB ingested
  getToMRead(1); // read MSB of dest
  getToMRead(0); // MSB ingested
  digitalWrite(DBDRIVE,HIGH); // stop driving the databus - let Z80 place data there
                              // WR# and MREQ# are still blocked :)
  getToMWrite(1); // wait till Z80 places PChigh on databus
  int pcHigh=readDataBus();
  getToMWrite(0);

  getToMWrite(1); // wait till Z80 places PClow (+3) on databus
  int pcLow=readDataBus()-3; // return address is orig PC + 3
  getToMWrite(0); // Z80 is done writing old PC(+3)
  getToOCFetch(1); // next instruction (random)

// need to restore SP
  writeDataBus(0xC9); // RETurn instruction
  claimDataBus(); // let Z80 read RET instruction
  getToOCFetch(0); // opcode ingested
  getToOCFetch(1); // RET is finished (jumped to random location)
  
  releaseDataBus();

// find actual PC
  int PC=(pcHigh<<8) | pcLow;

// let's read the registers :)
  RegB=readReg(0x70);
  RegC=readReg(0x71);
  RegD=readReg(0x72);
  RegE=readReg(0x73);
  RegH=readReg(0x74);
  RegL=readReg(0x75);
  RegA=readReg(0x77);

// read F flag
// use PUSH AF (0xF5) to examine this
  getToOCFetch(1); // Z80 is trying to fetch an instruction
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(0xF5); // opcode for PUSH AF
  claimDataBus();
  getToOCFetch(0); // wait till CALL has been ingested
  digitalWrite(DBDRIVE,HIGH); // stop driving the databus - let Z80 place data there
                              // WR# and MREQ# are still blocked :)
  getToMWrite(1); // wait till Z80 places PChigh on databus
  //int AReg=readDataBus(); // A register is on the databus. We already have this
  getToMWrite(0);getToMWrite(1);
  RegF=readDataBus(); // save in evil global :)
  getToOCFetch(1); // Done with instruction...back up the SP
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(0xC9); // opcode for RET
  claimDataBus();
  getToOCFetch(0); // wait till RET has been ingested
  digitalWrite(DBDRIVE,HIGH); // stop driving the databus - let Z80 place data there
  getToOCFetch(1); // ready for next instruction (PC is all messed up)

// read SP
// write LD (00),SP (ED 73 00 00)
// 2 WR cycles show SP (Low,High?)
  getToOCFetch(1); // already here!
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(0xED); // opcode for   LD (00),SP
  claimDataBus();
  getToOCFetch(0); // wait till OC has been ingested

  getToOCFetch(1);writeDataBus(0x73);getToMRead(0); // This looks like an M1 cycle!
  getToMRead(1);writeDataBus(0x00);getToMRead(0);
  getToMRead(1);writeDataBus(0x00);getToMRead(0);

  digitalWrite(DBDRIVE,HIGH); // stop driving the databus - let Z80 place data there
                              // WR# and MREQ# are still blocked :)

  getToMWrite(1); // wait till Z80 places register on databus
  int SPLow=readDataBus(); // this is the register's value that we're seeing on the dbus
  getToMWrite(0);
  getToMWrite(1); int SPHigh=readDataBus(); getToMWrite(0);

  getToOCFetch(1); // get to next instruction
  releaseDataBus(); // let Z80 control mem signals again

  RegSP=(SPHigh<<8) | SPLow;

// go back to original PC :)
  jump(PC);

  return(PC);
}

// after reading the PC, let's read the registers and stuff
int readReg(int OC)
{
  getToOCFetch(1); // already here!
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(OC); // opcode for   LD (HL),desired-reg
  claimDataBus();
  getToOCFetch(0); // wait till OC has been ingested
  digitalWrite(DBDRIVE,HIGH); // stop driving the databus - let Z80 place data there
                              // WR# and MREQ# are still blocked :)
  getToMWrite(1); // wait till Z80 places register on databus
  int retval=readDataBus(); // this is the register's value that we're seeing on the dbus
  getToMWrite(0);
  getToOCFetch(1); // get to next instruction
  releaseDataBus(); // let Z80 control mem signals again
  return(retval);
}

// write into a register
void setReg(int OC,int n) // synthesize a "LD reg,n" instruction
{
  getToOCFetch(1);
  digitalWrite(BLOCK,HIGH); // CPU isn't allowed to read or write SRAM
  writeDataBus(OC); // opcode for   LD (HL),desired-reg
  claimDataBus();
  getToOCFetch(0); // wait till OC has been ingested
  getToMRead(1); // wait till Z80 places register on databus
  writeDataBus(n); // specify operand
  getToMRead(0);
  getToOCFetch(1); // get to next instruction
  releaseDataBus(); // let Z80 control mem signals again
// backup to prior instruction
  backup(2);
}

// see what's happening
// analyze Z80 memory bus signals

// see if there is a #RD or #WR active
int memRequest()
{
  srLoad(); // load data and return 1st value (MREQ)
  if (!srShift())  return(1); // WR is low
  if (!srShift())  return(1); // RD is low
  return(0);
}

int OCFetch() // opcode fetch request
{
  int mreq=srLoad();
  int wr=srShift();
  int rd=srShift();
  int m1=srShift();

  return((m1+mreq+rd == 0)?1:0); // all are low (active)
}

// make sure this memory read is NOT an M1 cycle
// multi-byte opcodes (e.g. SRL = CB 3F) will request
// memory after 
// but this isn't really the issue...

int mRD() // Z80 memory read request
{
  int mreq=srLoad();
  int wr=srShift();
  int rd=srShift();
  int m1=srShift();
//Serial.print("mreq wr rd m1=");Serial.print(mreq);Serial.print(wr);Serial.print(rd);Serial.println(m1);
  return(((m1==1) && (mreq+rd==0))?1:0);
}

int mWR() // Z80 memory write request
{
  int mreq=srLoad();
  int wr=srShift();
  return((mreq+wr==0)?1:0);
}

// support code for moving through Z80 states
// flag is 1 to seek that state, 0 to seek NOT that state
// getToOCFetch(flag) - M1 and MREQ and RD
// getToMRead(flag) - MREQ and RD
// getToMWrite(flag) - MREQ and WR

// get to opcode fetch (nop if already there)
void getToOCFetch(int flag)
{
  int tries=0;
  while ((++tries < 1000) && (flag!=OCFetch())){
    doTick(0);
  }
  errorCheck(3,tries);
  if (flag==0) return;

  return;
}

// see if an opcode is a multi-byte opcode prefix
int MBOpcode(int oc)
{
  if ((oc==0xCB) || (oc==0xED) || (oc==0xDD) || (oc==0xFD)) return(1);
  return(0);
}

// are we at a safe place for manipulating the CPU?
int MBSafe()
{
  if ((MBFlag==0) && (OCFetch())) return(1); // SAFE!
  return(0);
}

// get to memory READ (nop if already there)
void getToMRead(int flag)
{
  int tries=0;
  while ((++tries < 1000) && (flag!=mRD())){
    doTick(0);
  }
  errorCheck(4,tries);
}

// get to memory WRITE (nop if already there)
void getToMWrite(int flag)
{
  int tries=0;
  while ((++tries < 1000) && (flag!=mWR())){
    doTick(0);
  }
  errorCheck(5,tries);
}

// display bus data
void showSignals(int flag)
{
  char temp[48];

  if (flag==BUSNONEFLAG) return;

// read hardware bus signals
  int mreq=1-srLoad();
  int wr=1-srShift();
  int rd=1-srShift();
  int m1=1-srShift();
  for (int i=0;i<8;i++) srShift(); // skip switches
  int halt=1-srShift(); // complemented
  int dbus=readDataBus(); // always safe?

// header
  Serial.print(F("M1 MRQ RD WR HLT DBUS"));
  if (flag==BUSALLFLAG) Serial.print(F("  F  A  B  C  D  E  H  L  SP   PC "));
  Serial.println();

// data
  space(1);
  dec(m1);
  space(2);
  dec(mreq);
  space(3);
  dec(rd);
  space(2);
  dec(wr);
  space(2);
  dec(halt);
  space(3);
  hex2(dbus);
  space(2);
  //sprintf(temp," %d  %d   %d  %d  %d   %02X  ",
               //m1,mreq,rd,wr,halt,dbus);
  //Serial.print(temp);
  if (flag==BUSALLFLAG){
    int PC=readPC(); // also reads registers and stuff :)
    hex2(RegF);space(1);
    hex2(RegA);space(1);
    hex2(RegB);space(1);
    hex2(RegC);space(1);
    hex2(RegD);space(1);
    hex2(RegE);space(1);
    hex2(RegH);space(1);
    hex2(RegL);space(1);
    hex4(RegSP);space(1);
    hex4(PC);space(1);
    //sprintf(temp,"%02X %02X %02X %02X %02X %02X %02X %02X %04X %04x",
            //RegF,RegA,RegB,RegC,RegD,RegE,RegH,RegL,RegSP,PC);
    //Serial.print(temp);Serial.print(" ");

// display disassembled instruction
    int b[5];
    for (int j=0;j<5;j++){
      b[j]=readDataBus();
      nextLoc();
    } // buffer of bytes!
    char *out=disassem(b);
// backup using JR instruction
     backup(5); // don't skip this instruction!
   //backup(5-getILen());
    Serial.print(out);
  }
  Serial.println();

  return;
}

void space(int n)
{
  for (int i=0;i<n;i++) Serial.print(" ");
}

void dec(int n)
{
  Serial.print(n);
}

void hex2(int n)
{
  char temp[3];
  sprintf(temp,"%02X",n);
  Serial.print(temp);
}

void hex4(int n)
{
  char temp[5];
  sprintf(temp,"%04X",n);
  Serial.print(temp);
}

void showRegs() // full register display
{
  char temp[48];

  Serial.println(F("F  A  B  C  D  E  H  L   SP   PC "));
  int PC=readPC(); // load global vars with register values
  hex2(RegF);space(1);
  hex2(RegA);space(1);
  hex2(RegB);space(1);
  hex2(RegC);space(1);
  hex2(RegD);space(1);
  hex2(RegE);space(1);
  hex2(RegH);space(1);
  hex2(RegL);space(1);
  hex4(RegSP);space(1);
  hex4(PC);
  //sprintf(temp,"%02X %02X %02X %02X %02X %02X %02X %02X %04X %04x",
          //RegF,RegA,RegB,RegC,RegD,RegE,RegH,RegL,RegSP,PC);
  //Serial.println(temp);
}

void help()
{
  Serial.println(F("run              - free-running mode"));
  Serial.println(F("stop             - stop running"));
  Serial.println(F("s                - step one M cycle"));
  Serial.println(F("t [n]            - tick processor [n times]. Each tick is a low/high cycle"));
  Serial.println(F("e addr           - examine an address"));
  Serial.println(F("e                - examine next"));
  Serial.println(F("i                - examine instruction"));
  Serial.println(F("d data           - deposit data and move to next location"));
  Serial.println(F("bus all/sig/none - indicate what bus data to display"));
  Serial.println(F("dump start end   - display contents of memory"));
  Serial.println(F("set reg value    - Load given value into register"));
  Serial.println(F("regs             - show contents of registers"));
  Serial.println(F("reset            - reset CPU"));
  Serial.println(F("write file s e   - write SRAM (pages s to e) to flash file"));
  Serial.println(F("read file s e    - read SRAM (pages s to e) from flash file"));
  Serial.println(F("?                - get help message"));
  Serial.println(F("blank line       - repeat prior command"));
  Serial.println("");
}

/*
 * command processor
 * Commands are:
 * run - free-running mode
 * stop - stop the run
 * s - step one M cycle
 * t [n] - tick processor [n times]. Each tick is a low/high cycle
 * e addr - examine an address
 * e - examine next
 * i - show instruction
 * d data - deposit data and move to next location
 * bus [all | none] - indicate what bus data to display
 * dump start end - display contents of memory
 * set reg val - load value into register :)
 * regs - show contents of registers
 * reset - reset CPU
 * write file startpage endpage  - write SRAM (pages startpage to endpage) to flash file
 * read file startpage endpage  - read SRAM (pages startpage to endpage) from flash file
 * ? - get help message
 * blank line - repeat prior command
 *
 */

void process(char *cmd)
{
  char p1[32],p2[32];
  int n;
  long start,end,file; // temp parse vars

  if (cmd[0]=='\0') strcpy(cmd,lastCmd); // re-use old commands
  strcpy(lastCmd,cmd); // and save this one

// parse the command

// multi-letter commands first
  if (1==sscanf(cmd,mycvt(F("bus %s")),p1)){ // indicate how often bus data should be displayed
    if (0==strcmp(p1,"all")) busShowFlag=BUSALLFLAG;
    else if (0==strcmp(p1,"sig")) busShowFlag=BUSSIGFLAG;
    else if (0==strcmp(p1,"none")) busShowFlag=BUSNONEFLAG;
    else help();
    return;
  }

  if (2==sscanf(cmd,mycvt(F("dump %lx %lx")),&start,&end)){ // display memory
    if (runCheck()) return;
    for (long i=start&0xfff0;i<=end;i++){ // display an entire row
      doDump(i);
    }
    Serial.println("");
    return;
  }

// run/stop/reset
  if (0==strcmp(cmd,"run")){
    doRun();
    return;
  }

  if (0==strcmp(cmd,"stop")){
    doStop();
    return;
  }

  if (0==strcmp(cmd,"reset")){
    doReset();
    return;
  }

// set a register - hahahaha
  if (2 == sscanf(cmd,mycvt(F("set %s %x")),p1,&n)){
    setNamedReg(p1,n); // figure out opcode to load into given register
    return;
  }

// display registers' contents
  if (0==strcmp(cmd,"regs")){
    showRegs();
    return;
  }

// special load command - fed from external loader program
  if (0==strcmp(cmd,"load")){ // read 4 hex char address, followed by data, followed by ZZ
    doLoad();
    return;
  }

// FLASH read/write
  if (3 == sscanf(cmd,mycvt(F("read %lx %lx %lx")),&file,&start,&end)){
    IOFlash(READFLAG,file,start,end);
    return;
  }

  if (3 == sscanf(cmd,mycvt(F("write %lx %lx %lx")),&file,&start,&end)){
    IOFlash(WRITEFLAG,file,start,end);
    return;
  }

  switch (cmd[0]){
    case 't': // tick
             if (runCheck()) return; // don't do these types of things while system is running
             if (1==sscanf(cmd,"t %d",&n)){
               for (int i=0;i<n;i++){
                 doTick(1);
               }
             } else if (cmd[1]=='\0'){ // kosher
               doTick(1); // or just one
             } else {
               help();return;
             }
             if (busShowFlag == BUSNONEFLAG) return;
             if (busShowFlag == BUSSIGFLAG){
               showSignals(busShowFlag);
               return;
             }

// sometimes we can show things, even during a 't' command
             if (!MBSafe()){
               showSignals(BUSSIGFLAG); // don't show PC
               return;
             }
             showSignals(BUSALLFLAG);
             break;

    case 's': // single step
             if (cmd[1]!='\0'){help();return;} // syntax error
             if (runCheck()) return;
             MStep(1); // show signals is requested
             break;

    case 'S': // step till safe
             if (cmd[1]!='\0'){help();return;} // syntax error
             if (runCheck()) return;
             M1Step();
             break;

    case 'e': // examine memory (put data on bus)
             if (runCheck()) return;
             if (1==sscanf(cmd,"e %x",&n)){
               jump(n);
             } else if (cmd[1]=='\0'){
               nextLoc();
             } else {help();return;}
             n=readDataBus();
             hex2(n);Serial.println("");
             //sprintf(p1,"%02X",n);
             //Serial.println(p1);
             break;

    case 'd': // deposit at current location
              // also move to next location (not that DepNext switch does addr inc FIRST)
             if (runCheck()) return;
             if (1==sscanf(cmd,"d %x",&n)){
               //jump(something!);
               load(n);
               nextLoc();
             } else {
               help();
             }
             break;
    case 'i': // disassem
             if (runCheck()) return;
// read PC
             if (1!=sscanf(cmd,"i %d",&n)){ // just 'i'?
               if (cmd[1]!='\0'){help();return;}
               n=1; // if just 'i' then do one instruction
             }

             for (int i=0;i<n;i++){ // show instruction
               int b[5];
               for (int j=0;j<5;j++){
                 b[j]=readDataBus();
                 nextLoc();
               } // buffer of bytes!
               char *out=disassem(b);
// backup using JR instruction
               backup(5-getILen());
               int PC=readPC(); // see where we are
               sprintf(p1,"%04X: ",PC-getILen()); // current PC-length of last inst=addr of last instr
               Serial.print(p1);
               Serial.println(out);
             } // all instructions done
             break;

    case '?': help();break;

    default: Serial.println(F("Enter ? for help"));
  }
}

void setNamedReg(char *regName, int val)
{
  char reg;
  int OC;

  if (regName[1] != '\0'){help();return;} // must be one char (for now)
  reg=regName[0];
  switch (reg){
    case 'a':case 'A':OC=0x3E;break;
    case 'b':case 'B':OC=0x06;break;
    case 'c':case 'C':OC=0x0E;break;
    case 'd':case 'D':OC=0x16;break;
    case 'e':case 'E':OC=0x1E;break;
    case 'h':case 'H':OC=0x26;break;
    case 'l':case 'L':OC=0x2E;break;
    default:Serial.print(F("Unknown register: "));Serial.println(reg);return;
  }
  setReg(OC,val); // synthesize a "LD reg,val" instruction
}

// there are some commands we don't want to do while the system is running
int runCheck()
{
  if (running){
    Serial.println(F("\Stop the system ('stop') before doing this\n"));
    return(1);
  }
  return(0);
}

void doDump(long loc)
{
  // display contents of location loc
  char temp[32];

  if (runCheck()) return;

  jump((int) loc); // should leave data on the bus
  int data=readDataBus();
  if ((loc&0x000f)==0){
    hex4(loc);Serial.print(F(" : "));hex2(data);space(1);
    //sprintf(temp,"%04X : %02X ",(int) loc,data);
  } else {
    hex2(data);space(1);
    //sprintf(temp,"%02X ",data);
  }
  //Serial.print(temp);

// display ascii codes :)
  if ((loc&0x0f) == 15){ // end of line // note the parens seem to be required!!!
    for (long i=loc&0xfff0;i<=loc;i++){
      jump((int) i);data=readDataBus();
      if ((data >= 32) && ( data < 127)){
        char c=data;
        Serial.print(c);
      } else Serial.print(F("."));
    }
    Serial.println("");
  }
}

// change mode of system
// call this to stop free-run mode and let Arduino control the system for a bit
void doStop()
{
  digitalWrite(CPUXTAL,HIGH); // should already be there
  digitalWrite(XBLOCK,HIGH);
  delay(50); // let the clock-FF go high
  if (!running) return;

// we are running and want to stop
  running=0;
  getToOCFetch(1); // let's get to an M1 cycle :)
  MBFlag=2; // require two OCFetches to be safe
}

// call doRun() to return the system to free-run mode
void doRun()
{
  if (running==1) return;
  getToOCFetch(1); // wait for M1
  releaseDataBus();
  digitalWrite(CPUWR,HIGH);
  digitalWrite(BLOCK,LOW); // let memory bus signals be managed by the Z80
  digitalWrite(CPUXTAL,HIGH); // will help xtal flow through
  digitalWrite(XBLOCK,LOW);
  running=1;
}

// code for a pair of nested countdown loops with a slow increment of output byte
void specialDebug()
{
  jump(0);
  load(0xd3);nextLoc();
  load(0xff);nextLoc();
  load(0xcd);nextLoc();
  load(0x09);nextLoc();
  load(0x00);nextLoc();
  load(0x3c);nextLoc();
  load(0xc3);nextLoc();
  load(0x00);nextLoc();
  load(0x00);nextLoc();
  load(0x05);nextLoc();
  load(0xc2);nextLoc();
  load(0x09);nextLoc();
  load(0x00);nextLoc();
  load(0x0d);nextLoc();
  load(0xc2);nextLoc();
  load(0x09);nextLoc();
  load(0x00);nextLoc();
  load(0xc9);nextLoc();
  jump(0);

  doRun();
  return;
}

// debouncing
// Wait a bit after a switch changes
// then save the position
// IGNORE switches if they're in the same position as last time
#define dbtime 200 // debounce time, mSec

int lastEXSW=0,lastDEPSW=0, lastDEPNEXTSW=0, lastRESETSW=0;
int lastRUNSW=0, lastSTOPSW=0, lastSTEPSW=0, lastEXNEXTSW=0; // 1 means ACTIVE

// read switch positions, debounce, and call code as needed
void processSwitches()
{
// first load all switches :)
  srLoad();srShift();srShift();srShift(); // last call returns M1
  int depnext=srShift();
  int dep=srShift();
  int exnext=srShift();
  int ex=srShift();
  int step=srShift();
  int stop=srShift();
  int run=srShift();
  int reset=srShift();

// special flash commands
// DEP and STOP mean LOAD from flash into SRAM
// EX and STOP mean SAVE from SRAM to flash

  if (!stop && !ex){
    SRAMToFlash();
    return;
  }

  if (!stop && !dep){
    flashToSRAM();
    return;
  }
  
  if (!dep){
    if (lastDEPSW) return;
    noRun();
    Serial.println(F("DEPOSIT"));
    swLoad(); // read switches, deposit into memory :)
    lastDEPSW=1;
    delay(dbtime);
    return;
  } else lastDEPSW=0;

  if (!depnext){
    if (lastDEPNEXTSW) return;
    noRun();
    Serial.println(F("DEP NEXT"));
    nextLoc();
    swLoad();
    lastDEPNEXTSW=1;
    delay(dbtime);
    return;
  } else lastDEPNEXTSW=0;

  if (!reset){
    if (lastRESETSW) return;
    Serial.println(F("RESET"));
    doReset();
    lastRESETSW=1;
    delay(dbtime);
    return;
  } else lastRESETSW=0;

  if (!run){
    if (lastRUNSW) return; // ignore button press if this is set
    Serial.println("RUN");
    doRun();
    lastRUNSW=1;
    delay(dbtime);
    return;
  } else lastRUNSW=0;

  if (!stop){
    if (lastSTOPSW) return;
    Serial.println(F("STOP"));
    doStop();
    lastSTOPSW=1;
    delay(dbtime);
    return;
  } else lastSTOPSW=0;

// The following commands require us to be in non-free-running mode

  if (!step){
    if (lastSTEPSW){ // check for long-press
      if (checkTime() > 3000){
        Serial.println(F("TICK DELTA"));
        stepMode=(++stepMode)%3;
        if (stepMode==STEP_T) alert(0x01,10);
        if (stepMode==STEP_M) alert(0x0f,10);
        if (stepMode==STEP_M1) alert(0xff,10);
        saveTime(); // reset long-press checker
      }
      return;
    }
    noRun();
    Serial.println(F("STEP"));
    saveTime(); // remember when this was pressed
    singleStep(); // uses stepMode to decide granularity
    lastSTEPSW=1;
    delay(dbtime);
    return;
  } else {
    lastSTEPSW=0;
  }

  if (!exnext){
    if (lastEXNEXTSW) return; // already responded to this!
    noRun();
    Serial.println(F("EX NEXT"));
    nextLoc(); // just move to next address
    lastEXNEXTSW=1;
    delay(dbtime);
    return;
  } else lastEXNEXTSW=0;

  if (!ex){
    if (lastEXSW) return;
    noRun();
    Serial.println(F("EXAMINE"));
    swJump(); // go to address specified by switches
    lastEXSW=1;
    delay(dbtime);
    return;
  } else lastEXSW=0;
  
}

// some switch operations should cause an exit from free-run mode
void noRun()
{
  if (running){
    Serial.println(F("Leaving free-run mode"));
    doStop();
  }
}

// reset the Z80
void doReset()
{
// we are low on GPIO, so we use DBIN + SRSHIFT to drive #RESET
  digitalWrite(DBIN,LOW);digitalWrite(SRLOAD,LOW); // yeah!
  if (!running){for (int i=0;i<30;i++) doTick(0);} // let this percolate through the system
  digitalWrite(DBIN,HIGH);digitalWrite(SRLOAD,HIGH); // release reset
}

   //                  \\
  // *** FLASH CODE *** \\
 //                      \\

#include<SPIMemory.h>
#define BAUD_RATE 115200

SPIFlash flash; // the flash memory

// one-time init...connect, get info, etc.
void flashInit()
{
  flash.begin();
  if (flash.error()) {
    Serial.println(flash.error(VERBOSE));
  }

// find flash info

  uint8_t b1, b2, b3;
  uint32_t JEDEC = flash.getJEDECID();
  b1 = (JEDEC >> 16);
  b2 = (JEDEC >> 8);
  b3 = (JEDEC >> 0);
  //Serial.print("Man: ");Serial.print(b1,HEX);
  //Serial.print(" Type: ");Serial.print(b2,HEX);
  //Serial.print(" Cap: ");Serial.print(b3,HEX);
  //Serial.print(" JEDEC ID: ");Serial.println(JEDEC,HEX); // from dev
  //Serial.print("Capacity: ");Serial.println(flash.getCapacity());
}

// my wrappers for read/write/erase

int flashRead(long addr)
{
  return (flash.readByte(addr));
}

void flashWrite(long addr, int data)
{
  if (!flash.writeByte(addr, data)){
    Serial.println(F("Flash write error - erase issue?"));
  }
}

void flashErase(long addr)
{
  flash.eraseSector(addr);
}

// front-panel-activated flash requests
// STOP + EX:  SRAM->FLASH
// STOP + DEP: FLASH->SRAM
//
// Reads/writes from 1-16 pages; each page is 4K
// page n is at addresses 0xn000-0xnfff
//
// Addressing is driven by address switches
//
// ADDR<5:0>=file number (0-63)
// ADDR<B:8>=starting page (0-15)
// ADDR<F:C>=ending page EXCLUSIVE (0-15) If same as starting page, do all 16 pages

// Read from SRAM and save in flash (STOP+EX)
void SRAMToFlash()
{
  int fileNum=readAddr(SW70EN);
  int pageSel=readAddr(SWF8EN);
  IOFlash(WRITEFLAG,fileNum&0x3f,(pageSel&0x0f),((pageSel>>4)&0x0f));
  //alert(0xff,5);
}

void flashToSRAM()
{
  int fileNum=readAddr(SW70EN);
  int pageSel=readAddr(SWF8EN);
  IOFlash(READFLAG,fileNum&0x3f,(pageSel&0x0f),((pageSel>>4)&0x0f));
  //alert(0xff,5);
}

// read address switches
int readAddr(int which) // which is SW70EN or SWF8EN
{
  getToOCFetch(1); // make sure Z80 is reading from mem
  digitalWrite(BLOCK,HIGH);   // cause #RD and #WR from Z80 to be ignored
                              // (but don't ask for DBDrive!)

  digitalWrite(which,LOW);   // place low bank of switches on databus
  int addr=readDataBus();
  digitalWrite(which,HIGH);  // remove switches from bus

  digitalWrite(BLOCK,LOW);    // return bus to Z80's control

  return(addr);
}

// shared code for reading and writing SRAM and flash
// RWFlag is with respect to flash (e.g. WRITEFLAG means write into flash)
void IOFlash(int RWFlag,long fileNum,long pageStart, long pageEnd)
{
  long flashLoc=fileNum<<16; // starting address of file in Flash
  long SRAMLoc=pageStart<<12; // where to start in SRAM
  flashLoc+=SRAMLoc; // add this offset to flash loc also

  doStop();

  jump((int)SRAMLoc); // goto starting loc in SRAM

// loop through all pages (PAGE=4K)
  if (pageEnd==pageStart) pageEnd+=16; // same page means do all 16
// not so weird
// start end meaning
//   0    0   all pages
//   0    1   page 0
//   0    2   pages 0-1
//   0    8   pages 0-7
//
//   5    6   page 5
//   8    c   pages 8-b
//
// but maybe a little weird with:
//   2    3   page 2
//   2    2   all pages
//   2    1   no pages
//

// save current page request
  int pageSel=readAddr(SWF8EN); // ending/starting page

  for (int page=pageStart;page < pageEnd;page++){
    Serial.print(F("\nPage "));Serial.print(page,HEX);

    if (RWFlag==WRITEFLAG){ // erase page
      //Serial.print("Erasing loc ");Serial.println(flashLoc,HEX);
      flashErase(flashLoc);
    }

  int data;
// sweep the page
    for (int i=0;i<4096;i++){
      if (i%64==0){
// check for abort signal
        if (pageSel!=readAddr(SWF8EN)){ // exit here
          alert(0xff,2);
          return;
        }
        Serial.print(F("."));
      }
// transfer between flash[flashLoc] and SRAM[SRAMLoc]
      //Serial.print(flashLoc,HEX);Serial.print(" - ");Serial.println(SRAMLoc,HEX);

      if (RWFlag==READFLAG){ // from flash to SRAM
        data=flashRead(flashLoc);
        //Serial.print("Read from ");Serial.print(flashLoc,HEX);
        //Serial.print("=");Serial.println(data,HEX);
        load(data);
      } else { // from SRAM to flash
        data=readDataBus();
        //Serial.print("Write to ");Serial.print(flashLoc,HEX);
        //Serial.print("<=");Serial.println(data,HEX);
        flashWrite(flashLoc,data);
      }
      nextLoc(); // move to next SRAM location
      ++flashLoc;++SRAMLoc; // bump our pointers. Note that we don't actually use SRAMLoc ;)
    } // end of page
  } // end of all pages
  Serial.println();
  alert(0xff,5);
}

// signaling via databus LEDs
void alert(int pattern,int count)
{
  getToOCFetch(1);
  claimDataBus();
  for (int i=0;i<count;i++){
    writeDataBus(pattern);
    delay(100);
    writeDataBus(0);
    delay(100);
  }
  releaseDataBus();
}

// read 4 hex char address, followed by 2 hex char bytes
// Z marks end of stream

void doLoad()
{
  int addr, data;

  doStop();

// readNext() reads 2 hex digits (MSB first) and returns its value as an int
// receive the address to write
  addr=readNext() << 8; // MSB
  addr=addr|readNext(); // LSB
  sendAck();

  jump(addr); // go there

  while (-1 != (data=readNext())){
    sendAck();
    load(data);
    nextLoc();
  }

// done loading - return to starting address
  sendAck(); // ack the ZZ

  alert(0xEE,5); // tell user we're done

  jump(addr); // jump to starting address
}

// read 2 hex digits, return an int
int readNext()
{
  int val;
  char buffer[3];
  while (!Serial.available());
  buffer[0]=Serial.read(); // ascii char
  while (!Serial.available());
  buffer[1]=Serial.read();
  buffer[2]='\0';
  if (1 != sscanf(buffer,"%x",&val)) return(-1);
  return(val);
}

void sendAck()
{
  Serial.print(F("."));
}

long lastTime=0;

// record current timestamp for future calls to checkTime()
// time-monitoring code
void saveTime()
{
  lastTime=millis();
}

// see how many mSec have passed since last call to saveTime()
int checkTime()
{
  return((int)(millis()-lastTime));
}

                        //*****************\\
                       //-------------------\\
                      //                     \\
                     //     DISASSEMBLER      \\
                    //_________________________\\
                   ////////              \\\\\\\\\
                  ////////                \\\\\\\\\
                 ////////   (good stuff)   \\\\\\\\\
                ////////                    \\\\\\\\\
               ////////                      \\\\\\\\\
              ////////========================\\\\\\\\\

// Based on the fantastic work of based on work of
// Cristian Dinu (www.z80.info/decoding.html)
// which sites the Romanian book "Ghidul Programatorului ZX Spectrum"
// ("The ZX Spectrum Programmer's Guide") plus other collaborators
//
// This is the easiest-to-use, most-complete and concise synopsis
// of Z80 disassembly I have encountered!


#include <string.h>
char *disassem(int,int,int,int,int); // main disassembler
char *r(int);
char *rp(int);
char *rp2(int);
char *cc(int);
char *alu(int);
char *rot(int);
char *im(int); // helper tables

int iLen; // set during disassembly
int getILen()
{
  return(iLen);
} // okay cool?

// tables

char *r(int ind) // 8-bit registers
{
  switch(ind){
    case 0: return("B");
    case 1: return("C");
    case 2: return("D");
    case 3: return("E");
    case 4: return("H");
    case 5: return("L");
    case 6: return("(HL)");
    case 7: return("A");
  }
}

char *cc(int ind) // condition codes
{
  switch(ind){
    case 0: return("NZ");
    case 1: return("Z");
    case 2: return("NC");
    case 3: return("C");
    case 4: return("PO");
    case 5: return("PE");
    case 6: return("P");
    case 7: return("M");
  }
}

char *alu(int ind) // alu ops
{
  switch(ind){
    case 0: return("ADD A,");
    case 1: return("ADC A,");
    case 2: return("SUB ");
    case 3: return("SBC A,");
    case 4: return("AND ");
    case 5: return("XOR ");
    case 6: return("OR ");
    case 7: return("CP ");
  }
}

char *rot(int ind) // Rotate/shift
{
  switch(ind){
    case 0: return("RLC");
    case 1: return("RRC");
    case 2: return("RL");
    case 3: return("RR");
    case 4: return("SLA");
    case 5: return("SRA");
    case 6: return("SLL");
    case 7: return("SRL");
  }
}

char *im(int ind) // Interrupt modes
{
  switch(ind){
    case 0: return("0");
    case 1: return("0/1");
    case 2: return("1");
    case 3: return("2");
    case 4: return("0");
    case 5: return("0/1");
    case 6: return("1");
    case 7: return("2");
  }
}

char *rp(int ind) // Register pairs
{
  switch(ind){
    case 0: return("BC");
    case 1: return("DE");
    case 2: return("HL");
    case 3: return("SP");
  }
}

char *rp2(int ind) // Register pairs with AF
{
  switch(ind){
    case 0: return("BC");
    case 1: return("DE");
    case 2: return("HL");
    case 3: return("AF");
  }
}

char bliTemp[6];
char *bli(int a, int b) // Block Instructions
{
  switch(b){  // form first part of opcode
    case 0: strcpy(bliTemp,"LD");break;
    case 1: strcpy(bliTemp,"CP");break;
    case 2: strcpy(bliTemp,"IN");break;
    case 3: strcpy(bliTemp,"OUT");break;
  }
// add suffix
  switch(a){
    case 4:strcat(bliTemp,"I");break;
    case 5:strcat(bliTemp,"D");break;
    case 6:strcat(bliTemp,"IR");break;
    case 7:strcat(bliTemp,"DR");break;
  }
  return(bliTemp);
}

char out[32]; // static
// main disassembler
char *disassem(int *bytes)
{

// CB and ED prefixes are handled inside disassemOB()
  if ((bytes[0]!=0xDD) && (bytes[0]!=0xFD)){ // okay running OB code
    strcpy(out,disassemOB(bytes));
    return(out);
  }

// Do some multi-byte processing here
// See if 2nd opcode is illegal
  if ((bytes[1]==0xDD) || (bytes[1]==0xED) || (bytes[1]==0xFD)){
    iLen=1;
    return(mycvt(F("NONI"))); // just skip the first prefix
  }

// record prefix (generally, DD=>IX, FD=>IY)
  char ixy[4];
  int IXFlag;
  if (bytes[0]==0xDD){
    IXFlag=1;
    strcpy(ixy,"IX");
  } else {
    IXFlag=0;
    strcpy(ixy,"IY");
  }

// DD CB?
  if (bytes[1] != 0xCB){ // no...
// decode from 2nd byte onward, then check for H, L, HL and (HL)
    bytes[0]=bytes[1];
    bytes[1]=bytes[2];
    bytes[2]=bytes[3];
    bytes[3]=bytes[4]; // simple shift
    strcpy(out,disassemOB(bytes));
    iLen++; // add one for initial prefix

// selective substitution
    if (contains(out,"(HL)")){ // re-order bytes and disassemble/replace again
      int dispByte=bytes[1]; // displacement byte
// If instruction included an immediate operand, we need to re-disassemble
// since dispByte was (incorrectly) seen as the immediate operand
      bytes[1]=bytes[2];bytes[2]=bytes[3];
      strcpy(out,disassemOB(bytes));
      iLen+=2; // account for prefix and displacement byte
      char dispstr[9];
      sprintf(dispstr,"(%s+%02X)",(IXFlag?"IX":"IY"),dispByte);
      replace(out,"(HL)",dispstr);return(out);
    } else if (contains(out,"HL")){ // replace with IX or IY
      if (0 == strcmp(out,"EX DE,HL")){ // special case - no change
        return(out);
      }
      replace(out,"HL",ixy);return(out);
    } else if (contains(out,"H")){ // replace with IXH or IYH
      if (IXFlag) strcpy(ixy,"IXH"); else strcpy(ixy,"IYH");
      replace(out,"H",ixy);return(out);
    } else if (contains(out,"L")){ // replace with IXL or IYL
      if (IXFlag) strcpy(ixy,"IXL"); else strcpy(ixy,"IYL");
      replace(out,"L",ixy);return(out);
    } else return(out); // no effect
  } // end of DD non-CB  or FD non-CB

// else this is a DD CB or FD CB instruction...
// format is e.g. DD CB dispbyte opcode
  char dispstr[9];
  sprintf(dispstr,"(%s+%02X)",(IXFlag?"IX":"IY"),bytes[2]);
  int x=(bytes[3]>>6)&0x3;
  int y=(bytes[3]>>3)&0x7;
  int z=bytes[3]&0x07;
  iLen=4;
  switch(x){
    case 0: if (z==6) sprintf(out,"%s %s",rot(y),dispstr);
            else sprintf(out,mycvt(F("LD %s,%s %s")),r(z),rot(y),dispstr);
            return(out);
    case 1: sprintf(out,mycvt(F("BIT %d,%s")),y,dispstr);
            return(out);
    case 2: if (z==6) sprintf(out,mycvt(F("RES %d,%s")),y,dispstr);
            else sprintf(out,mycvt(F("LD %s,RES %d,%s")),r(z),y,dispstr);
            return(out);
    case 3: if (z==6) sprintf(out,mycvt(F("SET %d,%s")),y,dispstr);
            else sprintf(out,mycvt(F("LD %s,SET %d,%s")),r(z),y,dispstr);
            return(out);
  }
  return(mycvt(F("???")));
}

// helper code for multi-byte situations
// see if string (after opcode) contains given string
int contains(char *in, char *find)
{
  int go=0; // set once we see first space
  for (int i=0;i<strlen(in);i++){
    if (in[i]==' ') go=1;
    if (!go) continue;
    int match=1;
    for (int j=0;j<strlen(find);j++){ // not too worried about overflow
      if (in[i+j] != find[j]) match=0; // :(
    }
    if (match) return(1);
  }
  return(0); // no match
}

// replace all occurrances (after opcode) of one string with another
void replace(char *str, char *from, char *to)
{
  char rout[32]; // replacement output
  int go=0; // set after opcode
  int outloc=0;

  int i=0; // read original string form here

  while (i<strlen(str)){
    if (str[i]==' '){
      go=1; // start paying attention
    }
    if (!go){ // no action yet
      rout[outloc++]=str[i++]; // copy opcode verbatim
      continue;
    }
// may need to do replacement here: let's see if input matches "from"
    int match=1; // clear on mismatch
    for (int j=0;j<strlen(from);j++){ // not too worried about overflow
      if (str[i+j] != from[j]) match=0; // not our pattern
    }
    if (!match){
      rout[outloc++]=str[i++];
      continue;
    }
// found target pattern...replace it
    for (int j=0;j<strlen(to);j++){
      rout[outloc++]=to[j];
    }
// now skip over the pattern we just replaced
    i+=strlen(from);
  }
// finish the string
  rout[outloc]='\0';
  strcpy(str,rout);
}

// one-byte-opcode disassem
char *disassemOB(int *bytes)
{
  int opcode,x,y,z,p,q,d,n,nn;

  iLen=1; // default...increment as needed
// no prefixes for now
  opcode=bytes[0];
  x=(opcode>>6)&0x3;
  y=(opcode>>3)&0x7;
  z=opcode&0x07;
  p=(y>>1)&0x03;
  q=y&0x01;
//Serial.print("OC=");Serial.print(opcode,HEX);Serial.print(" b2=");Serial.println(bytes[1],HEX);

  d=bytes[1]; // displacement
  n=bytes[1]; // immediate
  nn=bytes[1] | (bytes[2]<<8); // 16-bit immediate

  strcpy(out,"");

// let's do it...

  if (x==0){
    switch(z){
      case 0:
        switch(y){
          case 0:return("NOP");
          case 1:return(mycvt(F("EX AF, AF'")));
          case 2:sprintf(out,mycvt(F("DJNZ %02X")),d);iLen+=2;return(out);
          case 3:sprintf(out,mycvt(F("JR %02X")),d);iLen+=1;return(out);
          default:sprintf(out,mycvt(F("JR %s,%02X")),cc(y-4),d);iLen+=1;return(out);
        }
      case 1:
        switch(q){
          case 0:sprintf(out,mycvt(F("LD %s,%04X")),rp(p),nn);iLen+=2;return(out);
          case 1:sprintf(out,mycvt(F("ADD HL,%s")),rp(p));return(out);
        }
      case 2:
        switch(q){
          case 0:if (p==0) return(mycvt(F("LD (BC),A")));
                 if (p==1) return(mycvt(F("LD (DE),A")));
                 if (p==2) {sprintf(out,mycvt(F("LD (%04X),HL")),nn);iLen+=2;return(out);}
                 if (p==3) {sprintf(out,mycvt(F("LD (%04X),A")),nn);iLen+=2;return(out);}
          case 1:if (p==0) return(mycvt(F("LD A,(BC)")));
                 if (p==1) return(mycvt(F("LD A,(DE)")));
                 if (p==2) {sprintf(out,mycvt(F("LD HL,(%04X)")),nn);iLen+=2;return(out);}
                 if (p==3) {sprintf(out,mycvt(F("LD A,(%04X)")),nn);iLen+=2;return(out);}
        }
      case 3:
            if (q==0){sprintf(out,mycvt(F("INC %s")),rp(p));return(out);}
            if (q==1){sprintf(out,mycvt(F("DEC %s")),rp(p));return(out);}
      case 4:
            sprintf(out,mycvt(F("INC %s")),r(y));return(out);
      case 5:
            sprintf(out,mycvt(F("DEC %s")),r(y));return(out);
      case 6:
            sprintf(out,mycvt(F("LD %s,%02X")),r(y),n);iLen+=1;return(out);
      case 7:
        switch(y){
          case 0:return(mycvt(F("RLCA")));
          case 1:return(mycvt(F("RRCA")));
          case 2:return(mycvt(F("RLA")));
          case 3:return(mycvt(F("RRA")));
          case 4:return(mycvt(F("DAA")));
          case 5:return(mycvt(F("CPL")));
          case 6:return(mycvt(F("SCF")));
          case 7:return(mycvt(F("CCF")));
        }
    }
  } // end of x=0

  if (x==1){
    if ((y==6) && (z==6)) return(mycvt(F("HALT")));
    sprintf(out,"LD %s,%s",r(y),r(z));return(out);
  } // end of x=1

  if (x==2){
    sprintf(out,"%s%s",alu(y),r(z));return(out);
  } // end of x=2

  if (x==3){
    switch(z){
      case 0:sprintf(out,mycvt(F("RET %s")),cc(y));return(out);
      case 1:if (q==0) {sprintf(out,mycvt(F("POP %s")),rp2(p));return(out);}
             // q==1
             if (p==0) return(mycvt(F("RET")));
             if (p==1) return(mycvt(F("EXX")));
             if (p==2) return(mycvt(F("JP HL")));
             if (p==3) return(mycvt(F("LD SP,HL")));
      case 2:sprintf(out,mycvt(F("JP %s,%04X")),cc(y),nn);iLen+=2;return(out);
      case 3:switch (y){
                case 0: sprintf(out,mycvt(F("JP %04X")),nn);iLen+=2;return(out);
                case 1: // CB prefix
                  opcode=bytes[1];
                  iLen=2;
                  x=(opcode>>6)&0x3; y=(opcode>>3)&0x7; z=opcode&0x07;
                  switch(x){
                    case 0:sprintf(out,"%s %s",rot(y),r(z));return(out);
                    case 1:sprintf(out,mycvt(F("BIT %d %s")),y,r(z));return(out);
                    case 2:sprintf(out,mycvt(F("RES %d %s")),y,r(z));return(out);
                    case 3:sprintf(out,mycvt(F("SET %d %s")),y,r(z));return(out);
                  }
                case 2: sprintf(out,mycvt(F("OUT (%02X),A")),n);iLen+=1;return(out);
                case 3: sprintf(out,mycvt(F("IN A(%02X)")),n);iLen+=1;return(out);
                case 4: return(mycvt(F("EX (SP),HL")));
                case 5: return(mycvt(F("EX DE,HL")));
                case 6: return(mycvt(F("DI")));
                case 7: return(mycvt(F("EI")));
      }
      case 4:sprintf(out,mycvt(F("CALL %s,%04X")),cc(y),nn);iLen+=2;return(out);
      case 5:if (q==0){sprintf(out,mycvt(F("PUSH %s")),rp2(p));return(out);}
             // q=1
             if (p==0){sprintf(out,mycvt(F("CALL %04X")),nn);iLen+=2;return(out);}
             if (p==2){ // ED prefix
               opcode=bytes[1];
               iLen=2;
               x=(opcode>>6)&0x3;y=(opcode>>3)&0x7;z=opcode&0x07;
               p=(y>>1)&0x03;q=y&0x01;
               d=bytes[2]; // displacement
               n=bytes[2]; // immediate
               nn=bytes[2] | (bytes[3]<<8); // 16-bit immediate

               switch(x){
                 case 0:case 3: 
                   return(mycvt(F("NONI+NOP")));
                 case 2: // block instruction
                   if ((z<=3) && (y>=4)) return(bli(y,z));
                   else return(mycvt(F("NONI+NOP")));
                 case 1: // need a table
                   switch(z){
                     case 0:if (y==6) return(mycvt(F("IN (C)")));
                            sprintf(out,mycvt(F("IN %s,(C)")),r(y));return(out);
                     case 1:if (y==6) return(mycvt(F("OUT (C),0")));
                            sprintf(out,mycvt(F("OUT (C),%s")),r(y));return(out);
                     case 2:if (q==0) sprintf(out,mycvt(F("SBC HL,%s")),rp(p));
                            else sprintf(out,mycvt(F("ADC HL,%s")),rp(p));
                            return(out);
                     case 3:iLen+=2; // need "nn" 16-bit imm op
                            if (q==0) sprintf(out,mycvt(F("LD (%04X),%s")),nn,rp(p));
                            else sprintf(out,mycvt(F("LD %s,(%04X)")),rp(p),nn);
                            return(out);
                     case 4:return(mycvt(F("NEG")));
                     case 5:if (y==1) return(mycvt(F("RETI")));
                            return(mycvt(F("RETN")));
                     case 6:sprintf(out,mycvt(F("IM %s")),im(y));return(out);
                     case 7:
                       switch(y){
                         case 0: return(mycvt(F("LD I,A")));
			 case 1: return(mycvt(F("LD R,A")));
                         case 2: return(mycvt(F("LD A,I")));
                         case 3: return(mycvt(F("LD A,R")));
                         case 4: return(mycvt(F("RRD")));
                         case 5: return(mycvt(F("RLD")));
                         case 6: return(mycvt(F("NOP")));
                         case 7: return(mycvt(F("NOP")));
                       }
                   } // end of x=1 for ED prefix
                 }
             } // end of ED prefix

             if (p==1) return(mycvt(F("DD Prefix")));
             if (p==3) return(mycvt(F("FD Prefix")));
      case 6:sprintf(out,mycvt(F("%s %02X")),alu(y),n);iLen+=1;return(out);
      case 7:sprintf(out,mycvt(F("RST %02X")),y*8);return(out);
    }
  } // end of x=3
}

// can make normal (char* style) strings using mycvt(F("hahaha"))
char myout[32];
char *mycvt(const __FlashStringHelper* in)
{
  String s=String(in);
  int i;
  for (i=0;i<s.length();i++){
    myout[i]=s.charAt(i);
  }
  myout[i]='\0';
  if (i >= 32){
    Serial.print(in);
    Serial.println(F("*** LINE TOO LONG TO CVT! ***"));
  }
  return(myout);
}
